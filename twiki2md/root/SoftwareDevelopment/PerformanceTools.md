# Performance Tools

-   [Score-P](ScoreP) - tool suite for profiling, event tracing, and
    online analysis of HPC applications
-   [VampirTrace](VampirTrace) - recording performance relevant data at
    runtime
-   [Vampir](Vampir) - visualizing performance data from your program
-   [Hardware performance counters - PAPI](PapiLibrary) - generic
    performance counters
-   [perf tools](PerfTools) - general performance statistic
-   [IOTrack](IOTrack) - I/O statistics
-   [EnergyMeasurement](EnergyMeasurement) - energy/power measurements
    on taurus

-- Main.mark - 2009-12-16
