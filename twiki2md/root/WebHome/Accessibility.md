# Erklrung zur Barrierefreiheit

Diese Erklrung zur Barrierefreiheit gilt fr die unter
<https://doc.zih.tu-dresden.de> verffentlichte Website der Technischen
Universitt Dresden.

Als ffentliche Stelle im Sinne des Barrierefreie-Websites-Gesetz
(BfWebG) ist die Technische Universitt Dresden bemht, ihre Websites und
mobilen Anwendungen im Einklang mit den Bestimmungen des
Barrierefreie-Websites-Gesetz (BfWebG) in Verbindung mit der
Barrierefreie-Informationstechnik-Verordnung (BITV 2.0) barrierefrei
zugnglich zu machen.

## Erstellung dieser Erklrung zur Barrierefreiheit

Diese Erklrung wurde am 17.09.2020 erstellt und zuletzt am 17.09.2020
aktualisiert. Grundlage der Erstellung dieser Erklrung zur
Barrierefreiheit ist eine am 17.09.2020 von der TU Dresden durchgefhrte
Selbstbewertung.

## Stand der Barrierefreiheit

Es wurde bisher noch kein BITV-Test fr die Website durchgefhrt. Dieser
ist bis 30.11.2020 geplant.

## Kontakt

Sollten Ihnen Mngel in Bezug auf die barrierefreie Gestaltung auffallen,
knnen Sie uns diese ber das Formular [Barriere
melden](https://tu-dresden.de/barrierefreiheit/barriere-melden)
mitteilen und im zugnglichen Format anfordern. Alternativ knnen Sie sich
direkt an die Meldestelle fr Barrieren wenden (Koordinatorin: Mandy
Weickert, E-Mail: <barrieren@tu-dresden.de>, Telefon: [+49 351
463-42022](tel:+49-351-463-42022), Fax: [+49 351
463-42021](tel:+49-351-463-42021), Besucheradresse: Nthnitzer Strae 46,
APB 1102, 01187 Dresden).

## Durchsetzungsverfahren

Wenn wir Ihre Rckmeldungen aus Ihrer Sicht nicht befriedigend
bearbeiten, knnen Sie sich an die Schsische Durchsetzungsstelle wenden:

**Beauftragter der Schsischen Staatsregierung fr die Belange von
Menschen mit Behinderungen**\<br /> Albertstrae 10\<br /> 01097 Dresden

Postanschrift: Archivstrae 1, 01097 Dresden\<br /> E-Mail:
<info.behindertenbeauftragter@sk.sachsen.de>\<br /> Telefon: [+49 351
564-12161](tel:+49-351-564-12161)\<br /> Fax: [+49 351
564-12169](tel:+49-351-564-12169)\<br /> Webseite:
<https://www.inklusion.sachsen.de>

\<div id="footer"> \</div>

-- Main.MatthiasKraeusslein - 2020-09-18
